
public class EmployeeTest {

	/**
	 * @param args
	 */
	public static void main(String[] args) 
	{
		Employee emp1 = new Employee("anh", "hoang", 10);
		
		System.out.println("Luong NV emp1 la: " + emp1.getSalary() * 12);
		
		Employee emp2 = new Employee("huyen", "nguyen", 9);
		System.out.println("Luong NV emp2 la: " + emp2.getSalary() * 12);	
		emp1.setSalary(emp1.getSalary() * 1.1);
		System.out.println("Sau khi tang luong 10%, luong  emp1 la: " + emp1.getSalary() * 12); 
		emp2.setSalary(emp2.getSalary() * 1.1);
		System.out.println("Sau khi tang luong 10%, luong emp2 la: " + emp2.getSalary() * 12);
		
	}

}
